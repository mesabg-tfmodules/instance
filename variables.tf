variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "Instance name"
}

variable "ami" {
  type        = string
  description = "Instance AMI"
  default     = "ami-0a669382ea0feb73a"
}

variable "instance_type" {
  type        = string
  description = "Type of instance"
  default     = "t2.micro"
}

variable "key_name" {
  type        = string
  description = "Key Pair name"
  default     = null
}

variable "security_groups" {
  type        = list(string)
  description = "Security group IDs"
}

variable "subnet_id" {
  type        = string
  description = "Subnet Association ID"
}

variable "associate_public_ip" {
  type        = bool
  description = "Assign or not a public IP address"
  default     = false 
}

variable "ebs_optimized" {
  type        = bool
  description = "Launch with or without EBS optimized"
  default     = false
}

variable "user_data" {
  type        = string
  description = "Initializing command data"
  default     = ""
}

variable "volume_size" {
  type        = number
  description = "Volume size"
  default     = 8
}

variable "monitoring" {
  type        = bool
  description = "Launch with monitoring"
  default     = false
}
