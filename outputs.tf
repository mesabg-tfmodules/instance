output "id" {
  value       = aws_instance.instance.id
  description = "Generated Instance Identifier"
}

output "arn" {
  value       = aws_instance.instance.arn
  description = "Generated Instance ARN"
}

output "public_dns" {
  value       = aws_instance.instance.public_dns
  description = "Generated Instance Public DNS"
}

output "public_ip" {
  value       = aws_instance.instance.public_ip
  description = "Generated Instance Public IP"
}

output "private_ip" {
  value       = aws_instance.instance.private_ip
  description = "Generated Instance Private IP"
}
