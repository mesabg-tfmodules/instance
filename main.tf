terraform {
  required_version = ">= 0.13.2"
}

resource "aws_instance" "instance" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  vpc_security_group_ids      = var.security_groups
  subnet_id                   = var.subnet_id
  associate_public_ip_address = var.associate_public_ip
  ebs_optimized               = var.ebs_optimized
  monitoring                  = var.monitoring
  user_data                   = var.user_data

  root_block_device {
    volume_size               = var.volume_size
  }

  tags = {
    Name                      = var.name
    Environment               = var.environment
  }
}
