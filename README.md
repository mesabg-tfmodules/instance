# Instance Module

This module is capable to generate a new EC2 instance according to given parameters.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `ami` - (optional) base ami (default, last Amazon Linux 2)
- `instance_type` - (optional) type of instance (t2.micro is default)
- `key_name` - (optional) valid key pair
- `security_groups` - security groups list
- `subnet_id` - subnet id
- `associate_public_ip` - (oprional) associate or not a public ip (default false)
- `ebs_optimized` - (optional) enable EBS Optimization (default false)
- `monitoring` - (optional) enable monitoring (default false)

Usage
-----

```hcl
module "instance" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/instance.git"

  environment         = "environment"

  name                = "instance name"

  ami                 = "ami-0a669382ea0feb73a"
  instance_type       = "t3a.micro"
  key_name            = "my_keypair"
  security_groups     = ["sg-xxxxx"]
  subnet_id           = "subnet-xxxx"
  associate_public_ip = true
  ebs_optimized       = true
  monitoring          = true
}
```

Outputs
=======

 - `instance` - Created instance resource


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
